﻿using Microsoft.EntityFrameworkCore;

namespace NotesApp.Models
{
    public class NotesAppContext : DbContext
    {
        public NotesAppContext(DbContextOptions<NotesAppContext> options): base(options)
        {
        }

        public DbSet<NotesApp.Models.Note> Note { get; set; }
    }
}