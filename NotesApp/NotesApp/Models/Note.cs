﻿using System;
namespace NotesApp.Models
{
    public class Note
    {
        public int id { get; set; }
        public int user_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public DateTime release_date { get; set; }
    }
}