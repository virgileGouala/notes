# README #

This README would normally document whatever steps are necessary to get your application up and running.

### [Projet Libre](https://modules-api.etna-alternance.net/3934/activities/20463/download/IDV-NET5/003/quest/Projet%20libre/public/IDV-NET5_Sujet.html)###

Le but de ce projet est de réaliser une application web complète, en utilisant les technologies ASP.Net Core, dont vous êtes libre de définir le sujet, tant qu’il respecte les contraintes suivantes:
Votre projet devra être composé de deux parties:

* Une API web, permettant de manipuler un ensemble de données cohérent (vous êtes libres de la nature des données), utilisant EntityFramework.
* Une application web MVC utilisant cette API.
* Les fonctionnalités suivantes devront (au minimum) être présentes dans votre projet:

Un système d'authentification complet, avec formulaire d'inscription, de connexion, et de gestion des comptes.
Une interface permettant de manipuler (ajouter, afficher, éditer, supprimer) des données de votre API, au travers de votre application, avec un système de pagination.
